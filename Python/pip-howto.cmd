
#setup-tools
sudo apt-get install python-setuptools

#usage
easy_install SomePackage

#PIP
#included natively > 2.7.9
# generic way > 2.6
wget  https://bootstrap.pypa.io/get-pip.py

# ubuntu host for current python
sudo apt-get install python-pip

#usage
python2   -m pip install SomePackage
