>>> nombres = range(10)
>>> nombres_pairs = []
>>> for nombre in nombres:
...     if nombre % 2 == 0: # garder uniquement les nombres pairs
...         nombres_pairs.append(nombre)
...
>>> print(nombres)
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
>>> print(nombres_pairs)
[0, 2, 4, 6, 8]


>>> nombres = range(10)
>>> sommes = []
>>> for nombre in nombres:
...     if nombre % 2 == 0:
...         somme = 0
...         for i in range(nombre):
...             somme += i
...         sommes.append(somme)
...
>>> print(sommes)
[0, 1, 6, 15, 28]
