#!/usr/bin/python
#-*- coding: utf8 -*-

import begin


@begin.start
@begin.convert(port=int)
def run(file, port=8080):
    print(file)
    print(port)
