#-*- coding: utf-8 -*-

import random

class Animal(object):
    _list = []

    def __init__(self):
        Animal._list.append(self)


class Chien(Animal):
    cri = "ouah"

    def __init__(self):
        super(Chien,self).__init__()

    def __new__(cls, *args, **kwargs):
        # Customized __new__ implementation here
        return super(Chien, cls).__new__(cls)

class Chat(Animal):
    cri = "miaouuuu"

    def __init__(self):
        super(Chat,self).__init__()

    def __new__(cls, *args, **kwargs):
        # Customized __new__ implementation here
        return super(Chat, cls).__new__(cls)

def proceed():
    a = [Chien, Chat]
    b = [ random.choice(a)().cri for i in range(50)]

    print len(Animal._list)



if __name__ == "__main__":
    proceed()
