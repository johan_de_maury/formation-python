"""

python motus.py fichier.txt


 - marche: 1
 - semee: 5
 - obstacles: 7
 - sont: 9
 - entreprises: 11
 - egoistes: 12
 - sans: 15
 - fin: 16
 - surgir: 17
 - beni: 22
 - soit: 23
 - bonne: 28
 - volonte: 29
 - charite: 35
 - se: 36
 - guide: 44
 - ombre: 49
 - mort: 52
 - car: 56
 - gardien: 60
 - son: 62
 - frere: 63
 - providence: 66
 - enfants: 68
 - egares: 69
 - abattrai: 71
 - alors: 72
 - bras: 74
 - terrible: 77
 - colere: 78
 - furieuse: 82
 - effrayante: 84
 - hordes: 87
 - impies: 88
 - pourchassent: 90
 - reduisent: 92
 - neant: 94
 - brebis: 96
 - dieu: 98
 - connaitras: 101
 - pourquoi: 102
 - eternel: 107
 - quand: 108
 - s: 111
 - abattra: 112
 - puissant: 117
 - annees: 121
 - repete: 124
 - enfoire: 127
 - entend: 130
 - meurt: 132
 - aussitot: 133
 - avais: 135
 - jamais: 136
 - cherche: 137
 - comprendre: 139
 - trouvais: 141
 - seulement: 142
 - en: 145
 - jetait: 146
 - avant: 150
 - flinguer: 152
 - mec: 154
 - puis: 156
 - matin: 158
 - ai: 160
 - vu: 161
 - quelque: 162
 - chose: 163
 - m: 165
 - reflechir: 168
 - seul: 171
 - coup: 172
 - me: 174
 - pourrait: 177
 - bien: 178
 - vouloir: 179
 - joli: 199
 - mm: 200
 - serait: 202
 - protecteur: 204
 - angoisse: 212
 - ou: 216
 - encore: 217
 - mieux: 218
 - monde: 233
 - lucifer: 239
 - mais: 248
 - rien: 249
 - n: 253
 - juste: 255
 - vrai: 259
 - faible: 266
 - suis: 270
 - tyrannie: 272
 - mechants: 274
 - essaie: 278
 - ringo: 279
 - prix: 281
 - effort: 284
 - harassant: 285
 - proteger: 287
 - malin: 21, 187
 - au: 31, 280
 - nom: 32, 104
 - faibles: 41, 289
 - qu: 42, 240
 - dans: 45, 207
 - vallee: 47, 209
 - larmes: 55, 215
 - une: 76, 80
 - vengeance: 81, 114
 - sur: 85, 109
 - toi: 110, 225
 - tout: 116, 251
 - dire: 148, 180
 - dis: 175, 245
 - es: 183, 264
 - vertueux: 3, 192, 228
 - oeuvre: 19, 185, 237
 - du: 20, 115, 186
 - homme: 26, 191, 227
 - berger: 39, 206, 223
 - a: 93, 138, 166
 - un: 153, 170, 283
 - moi: 195, 221, 276
 - les: 10, 86, 95, 288
 - fait: 14, 37, 119, 167
 - il: 24, 43, 57, 131
 - j: 70, 134, 159, 277
 - tu: 100, 182, 244, 263
 - mon: 103, 198, 203, 205
 - je: 123, 140, 173, 269
 - ce: 157, 201, 242, 256
 - c: 193, 219, 230, 260
 - d: 6, 48, 75, 79, 169, 282
 - le: 38, 59, 73, 222, 232, 265
 - des: 2, 40, 54, 67, 120, 214, 273
 - qui: 8, 30, 89, 128, 164, 234, 257
 - ca: 118, 125, 144, 149, 176, 247, 252
 - la: 0, 34, 46, 51, 65, 113, 208, 271
 - que: 13, 122, 143, 181, 189, 197, 243, 262, 268
 - l: 18, 25, 106, 126, 129, 184, 190, 211, 226, 236
 - est: 4, 58, 105, 194, 220, 231, 235, 241, 254, 258, 261
 - de: 27, 33, 50, 61, 97, 147, 151, 210, 238, 246, 250, 286
 - et: 53, 64, 83, 91, 99, 155, 188, 196, 213, 224, 229, 267, 275
"""
#!/usr/bin/python
# -*- coding:utf-8 -*-

from __future__ import with_statement
import sys
import re
import unicodedata
from collections import defaultdict

def proceed(filename):
  with open(filename) as f:
    texte = f.read()
    pass


  texte = texte.decode('utf-8')
  texte = unicodedata.normalize('NFKD', texte)

  texte = texte.strip()
  texte = texte.replace("’", " ")
  texte = texte.replace("œ", "oe")

  texte = texte.encode('ASCII', 'ignore')
  texte = " ".join(re.split(r'\W+',texte))
  texte = texte.strip().lower()

  dictword = defaultdict(list)
  for i,m in enumerate(re.finditer(r'\w+', texte)):
    dictword[m.group(0)].append(i)
    pass

  for key,value in sorted(dictword.iteritems(), key = lambda (k,v):(len(v),v)):
    print "%s: %s" % (key, ', '.join([str(x) for x in value]))


if __name__ == "__main__":
   proceed(sys.argv[1])
