# test version
python -m venv

#ubuntu
#included natively > 2.7.9
sudo apt-get install virtualenv

#generic
python - m install virtualenv

#create env
cd Work
virtualenv venv #venv - directory for python and modules versions of this env, could be everythong else

#enter venv
source ./venv/bin/activate

#test 
python -v

#PYTHONPATH
env | grep PYTHONPATH

#pythonicway
python
>>> import sys
>>> sys.path



