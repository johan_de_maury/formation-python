def decorateur(fn):
    decorateur.cpt =  0
    def wrapper( *args, **kw):
        decorateur.cpt +=10
        print("avant exc")
        tmp = fn(*args, **kw)
        decorateur.cpt -= 10
        return tmp
    return wrapper

@decorateur
def ajouter(a, b):
    return a + b
"""
ajouter(3, 4)
ajouter(3, 4)
ajouter(3, 4)
"""


def readfile(path):
    with OpenFile(path) as (f,_str):
        print path, f, _str
    print path, f
    print "end"



class OpenFile(object):
    """This class docstring shows how to use sphinx and rst syntax

    The first line is brief explanation, which may be completed with
    a longer one. For instance to discuss about its methods. The only
    method here is :func:`function1`'s. The main idea is to document
    the class and methods's arguments with

    - **parameters**, **types**, **return** and **return types**::

          :param arg1: description
          :param arg2: description
          :type arg1: type description
          :type arg1: type description
          :return: return description
          :rtype: the return type description

    - and to provide sections such as **Example** using the double commas syntax::

          :Example:

          followed by a blank line !

      which appears as follow:

      :Example:

      followed by a blank line

    - Finally special sections such as **See Also**, **Warnings**, **Notes**
      use the sphinx syntax (*paragraph directives*)::

          .. seealso:: blabla
          .. warnings also:: blabla
          .. note:: blabla
          .. todo:: blabla

    .. note::
        There are many other Info fields but they may be redundant:
            * param, parameter, arg, argument, key, keyword: Description of a
              parameter.
            * type: Type of a parameter.
            * raises, raise, except, exception: That (and when) a specific
              exception is raised.
            * var, ivar, cvar: Description of a variable.
            * returns, return: Description of the return value.
            * rtype: Return type.

    .. note::
        There are many other directives such as versionadded, versionchanged,
        rubric, centered, ... See the sphinx documentation for more details.

    Here below is the results of the :func:`function1` docstring.

    """
    def __init__(self, filename):
        self.file = filename

    def __enter__(self):
        self.obj = open(self.file)
        return (self.obj, "Toto")

    def __exit__(self, type, value, traceback):
        self.obj.close()


readfile("exo1.txt")

